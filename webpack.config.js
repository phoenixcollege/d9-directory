const path = require('path');

module.exports = {
    mode: 'development',
    entry: './resources/js/obf.js',
    output: {
        filename: 'obf.js',
        path: path.resolve(__dirname, 'js')
    }
};
