const {Obfuscate, Dom} = require('smorken-obfuscate');

const smob = new Obfuscate();
const smobdom = new Dom(smob);

document.addEventListener('click', function (event) {
    if (!event.target.matches('.obf-show')) {
        return;
    }
    event.preventDefault();
    smobdom.modifyElement(event.target, 'clear');
});
