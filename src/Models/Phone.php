<?php

namespace Drupal\pcc_directory\Models;

/**
 * @property string $phone
 */
class Phone extends BaseModel {

  public function getPhoneAttribute(): string {
    return $this->attributes['raw_phone'];
  }

}
