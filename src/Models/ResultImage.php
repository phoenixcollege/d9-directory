<?php

namespace Drupal\pcc_directory\Models;

use Psr\Http\Message\ResponseInterface;

/**
 * @property int $statusCode
 * @property string $contentType
 * @property mixed $image
 */
class ResultImage extends ResultBase {

  protected function convert(
    ResponseInterface $response,
    $content
  ): ResultBase {
    return $this->newInstance([
      'statusCode' => $response->getStatusCode(),
      'contentType' => $response->getHeader('Content-Type')[0] ?? 'image/png',
      'image' => $content,
    ]);
  }

  protected function getContent(ResponseInterface $response) {
    return $response->getBody()->getContents();
  }

}
