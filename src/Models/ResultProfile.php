<?php

namespace Drupal\pcc_directory\Models;

use Psr\Http\Message\ResponseInterface;

/**
 * @property \Drupal\pcc_directory\Models\Person $person
 */
class ResultProfile extends ResultBase {

  protected function convert(
    ResponseInterface $response,
    $content
  ): ResultBase {
    $data = [
      'person' => new Person($content['person'] ?? []),
    ];
    return $this->newInstance($data);
  }

}
