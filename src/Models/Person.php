<?php

namespace Drupal\pcc_directory\Models;

/**
 * @property int $id
 * @property string $job_flag
 * @property string $profile_link
 * @property string $full_name
 * @property \Drupal\pcc_directory\Models\Profile|null $profile
 * @property \Drupal\pcc_directory\Models\Intermediate[] $intermediates
 * @property \Drupal\pcc_directory\Models\Link[] $links
 * @property \Drupal\pcc_directory\Models\Phone[] $phones
 * @property \Drupal\pcc_directory\Models\Email[] $emails
 */
class Person extends BaseModel {

  public function getPrimaryIntermediate(): ?Intermediate {
    foreach ($this->intermediates as $intermediate) {
      if ($intermediate->primary_job) {
        return $intermediate;
      }
    }
    return $this->intermediates[0] ?? NULL;
  }

  public function setEmailsAttribute($value): void {
    $this->setArrayFromArray($value, Email::class, 'emails');
  }

  public function setIntermediatesAttribute($value): void {
    $this->setArrayFromArray($value, Intermediate::class, 'intermediates');
  }

  public function setLinksAttribute($value): void {
    $this->setArrayFromArray($value, Link::class, 'links');
  }

  public function setPhonesAttribute($value): void {
    $this->setArrayFromArray($value, Phone::class, 'phones');
  }

  public function setProfileAttribute($value): void {
    $this->attributes['profile'] = new Profile($value);
  }

}
