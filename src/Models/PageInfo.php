<?php

namespace Drupal\pcc_directory\Models;

/**
 * @property int $current_page
 * @property string $first_page_url
 * @property int $from
 * @property int $to
 * @property int $last_page
 * @property string $last_page_url
 * @property string|null $next_page_url
 * @property string $path
 * @property int $per_page
 * @property string|null $prev_page_url
 * @property int $total
 */
class PageInfo extends BaseModel {

}
