<?php

namespace Drupal\pcc_directory\Models;

use Drupal\pcc_directory\Contracts\ResultModel;
use Psr\Http\Message\ResponseInterface;

abstract class ResultBase extends BaseModel implements ResultModel {

  abstract protected function convert(ResponseInterface $response, $content): ResultBase;

  public function fromResponse(ResponseInterface $response): ResultModel {
    $content = $this->getContent($response);
    if ($content) {
      return $this->convert($response, $content);
    }
    return $this;
  }

  protected function getContent(ResponseInterface $response) {
    return json_decode((string) $response->getBody()->getContents(), TRUE);
  }

}
