<?php

namespace Drupal\pcc_directory\Models;

/**
 * @property string $name
 * @property \Drupal\pcc_directory\Models\Location $location
 */
class Group extends BaseModel {

  public function setLocationAttribute($value): void {
    $this->attributes['location'] = new Location($value);
  }

}
