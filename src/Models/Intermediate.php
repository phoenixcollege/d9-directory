<?php

namespace Drupal\pcc_directory\Models;

/**
 * @property bool $primary_job
 * @property bool $manages
 * @property string|null $familiar_name
 * @property \Drupal\pcc_directory\Models\Job $job
 * @property \Drupal\pcc_directory\Models\Department $department
 * @property \Drupal\pcc_directory\Models\Location $location
 * @property \Drupal\pcc_directory\Models\Group|null $group
 * @property \Drupal\pcc_directory\Models\Person|null $supervisor
 */
class Intermediate extends BaseModel {

  public function getSupervisorAttribute($value): void {
    $this->attributes['supervisor'] = new Person($value);
  }

  public function setDepartmentAttribute($value): void {
    $this->attributes['department'] = new Department($value);
  }

  public function setGroupAttribute($value): void {
    $this->attributes['group'] = $value ? new Group($value) : NULL;
  }

  public function setJobAttribute($value): void {
    $this->attributes['job'] = new Job($value);
  }

  public function setLocationAttribute($value): void {
    $this->attributes['location'] = new Location($value);
  }

}
