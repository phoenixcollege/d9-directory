<?php
namespace Drupal\pcc_directory\Models;

use Drupal\pcc_directory\Contracts\Model;

class BaseModel implements Model {

  protected $attributes = [];

  public function __construct(array $attributes = []) {
    $this->setAttributes($attributes);
  }

  public function __get(string $key) {
    return $this->getAttribute($key);
  }

  public function __set(string $key, $value): void {
    $this->setAttribute($key, $value);
  }

  public function __isset(string $key) {
    return isset($this->attributes[$key]);
  }

  public function getAttribute(string $key, $default = NULL) {
    $methodName = 'get' . ucfirst($key) . 'Attribute';
    if (method_exists($this, $methodName)) {
      return $this->$methodName();
    }
    return $this->attributes[$key] ?? $default;
  }

  public function isEmpty(): bool {
    foreach ($this->attributes as $k => $v) {
      if ($v) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function newInstance(array $attributes = []): Model {
    return new static($attributes);
  }

  public function setAttribute(string $key, $value): void {
    $methodName = 'set' . ucfirst($key) . 'Attribute';
    if (method_exists($this, $methodName)) {
      $this->$methodName($value);
    }
    else {
      $this->attributes[$key] = $value;
    }
  }

  public function setAttributes(array $attributes): void {
    foreach ($attributes as $k => $v) {
      $this->setAttribute($k, $v);
    }
  }

  protected function setArrayFromArray(
    array $values,
    string $className,
    string $attribute
  ): void {
    $inserts = [];
    foreach ($values as $value) {
      $inserts[] = new $className($value);
    }
    $this->attributes[$attribute] = $inserts;
  }

}
