<?php

namespace Drupal\pcc_directory\Models;

use Psr\Http\Message\ResponseInterface;

/**
 * @property \Drupal\pcc_directory\Models\Group $group
 * @property \Drupal\pcc_directory\Models\PageInfo $pageInfo
 * @property \Drupal\pcc_directory\Models\Person[] $people
 */
class ResultGroup extends ResultBase {

  protected function convert(
    ResponseInterface $response,
    $content
  ): ResultBase {
    $data = [
      'group' => new Group($content['group'] ?? []),
      'pageInfo' => new PageInfo($this->getPageInfoAttributes($content['people'] ?? [])),
      'people' => $this->getPeople($this->getPeopleArray($content)),
    ];
    return $this->newInstance($data);
  }

  protected function getPageInfoAttributes(array $data): array {
    $attrs = [];
    $keys = [
      'current_page',
      'first_page_url',
      'from',
      'to',
      'last_page_url',
      'next_page_url',
      'path',
      'per_page',
      'prev_page_url',
      'total',
    ];
    foreach ($keys as $key) {
      $attrs[$key] = $data[$key] ?? NULL;
    }
    return $attrs;
  }

  protected function getPeople(array $data): array {
    $people = [];
    foreach ($data as $person) {
      $people[] = new Person($person);
    }
    return $people;
  }

  protected function getPeopleArray(array $content): array {
    return $content['people']['data'] ?? ($content['people'] ?? []);
  }

}
