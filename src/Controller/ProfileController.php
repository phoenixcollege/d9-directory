<?php
namespace Drupal\pcc_directory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pcc_directory\Requests\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProfileController extends ControllerBase {

  /**
   * @var \Drupal\pcc_directory\Requests\Profile
   */
  protected $profileRequest;

  public function __construct(Profile $profileRequest) {
    $this->profileRequest = $profileRequest;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(Profile::class)
    );
  }

  public function fetch(int $id): array {
    return [
      '#theme' => 'pcc_directory_profile_template',
      '#resultProfile' => $this->profileRequest->fetch($id),
      '#id' => $id,
    ];
  }

}
