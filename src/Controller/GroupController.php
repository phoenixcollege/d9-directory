<?php
namespace Drupal\pcc_directory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pcc_directory\Requests\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GroupController extends ControllerBase {

  /**
   * @var \Drupal\pcc_directory\Requests\Group
   */
  protected $groupRequest;

  public function __construct(Group $groupRequest) {
    $this->groupRequest = $groupRequest;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(Group::class)
    );
  }

  public function fetch(int $id): array {
    $page = (int) (isset($_GET['page']) ? $_GET['page'] : 1);
    return [
      '#theme' => 'pcc_directory_group_template',
      '#resultGroup' => $this->groupRequest->fetch($id, ['page' => $page]),
      '#id' => $id,
    ];
  }

  public function simple(int $id): array {
    return [
      '#theme' => 'pcc_directory_group_simple_template',
      '#resultGroup' => $this->groupRequest->fetch($id, ['all' => 1]),
      '#id' => $id,
    ];
  }

}
