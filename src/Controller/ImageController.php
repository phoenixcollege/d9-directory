<?php
namespace Drupal\pcc_directory\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pcc_directory\Requests\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends ControllerBase {

  /**
   * @var \Drupal\pcc_directory\Requests\Image
   */
  protected $imageRequest;

  public function __construct(Image $imageRequest) {
    $this->imageRequest = $imageRequest;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(Image::class)
    );
  }

  public function fetch(int $id): Response {
    $response = new Response();
    $result = $this->imageRequest->fetch($id);
    $response->setStatusCode($result->statusCode);
    $response->headers->set('Content-Type', $result->contentType);
    $response->setContent($result->image);
    return $response;
  }

}
