<?php

namespace Drupal\pcc_directory\Enums;

interface Endpoints {

  const GROUP = 'pcc_directory_group_endpoint';

  const IMAGE = 'pcc_directory_image_endpoint';

  const PROFILE = 'pcc_directory_profile_endpoint';

}
