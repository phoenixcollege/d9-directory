<?php
namespace Drupal\pcc_directory\Contracts;

interface Model {

  public function getAttribute(string $key, $default = NULL);

  public function isEmpty(): bool;

  public function newInstance(array $attributes = []): Model;

  public function setAttribute(string $key, $value): void;

  public function setAttributes(array $attributes): void;

}
