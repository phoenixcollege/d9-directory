<?php

namespace Drupal\pcc_directory\Contracts;

use Psr\Http\Message\ResponseInterface;

interface ResultModel extends Model {

  public function fromResponse(ResponseInterface $response): ResultModel;

}
