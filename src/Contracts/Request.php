<?php

namespace Drupal\pcc_directory\Contracts;

interface Request {

  public function fetch(int $id, array $query = []): ?ResultModel;

}
