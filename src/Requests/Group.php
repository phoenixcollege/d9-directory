<?php

namespace Drupal\pcc_directory\Requests;

use Drupal\pcc_directory\Enums\Endpoints;
use Drupal\pcc_directory\Models\ResultGroup;

class Group extends BaseRequest {

  protected $endpoint = Endpoints::GROUP;

  protected $resultModelClass = ResultGroup::class;

}
