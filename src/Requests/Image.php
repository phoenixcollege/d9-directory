<?php

namespace Drupal\pcc_directory\Requests;

use Drupal\pcc_directory\Enums\Endpoints;
use Drupal\pcc_directory\Models\ResultImage;

class Image extends BaseRequest {

  protected $endpoint = Endpoints::IMAGE;

  protected $resultModelClass = ResultImage::class;

}
