<?php

namespace Drupal\pcc_directory\Requests;

use Drupal\pcc_directory\Enums\Endpoints;
use Drupal\pcc_directory\Models\ResultProfile;

class Profile extends BaseRequest {

  protected $endpoint = Endpoints::PROFILE;

  protected $resultModelClass = ResultProfile::class;

}
