<?php

namespace Drupal\pcc_directory\Requests;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\pcc_directory\Contracts\Request;
use Drupal\pcc_directory\Contracts\ResultModel;
use Drupal\pcc_directory\Enums\Endpoints;
use Drupal\pcc_directory\Models\ResultBase;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

abstract class BaseRequest implements Request {

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * @var \Drupal\Core\Site\Settings
   */
  protected $config;

  protected $endpoint = NULL;

  protected $resultModelClass = NULL;

  protected $urlSettings = [
    Endpoints::GROUP => 'endpoints.group',
    Endpoints::IMAGE => 'endpoints.image',
    Endpoints::PROFILE => 'endpoints.profile',
  ];

  public function __construct(
    ClientInterface $client,
    ConfigFactoryInterface $settings
  ) {
    $this->client = $client;
    $this->config = $settings->get('pcc_directory.settings');
  }

  public function fetch(int $id, array $query = []): ?ResultModel {
    $url = $this->getUrl($id, $query);
    if ($url) {
      return $this->getResultModel()->fromResponse($this->doRequest($url, $this->getOptions()));
    }
    return NULL;
  }

  protected function doRequest(string $url, array $options): ResponseInterface
  {
    return $this->client->request('GET', $url, $options);
  }

  protected function getOptions(): array {
    return [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ],
    ];
  }

  protected function getResultModel(): ResultModel {
    return new $this->resultModelClass;
  }

  protected function getUrl(int $id, array $query = []): string {
    $baseUrl = $this->config->get($this->urlSettings[$this->endpoint]);
    $baseUrl = str_replace('{{id}}', $id, $baseUrl);
    return Url::fromUri($baseUrl, ['query' => $query, 'https' => TRUE])
              ->toUriString();
  }

}
