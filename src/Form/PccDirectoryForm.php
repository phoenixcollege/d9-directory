<?php
namespace Drupal\pcc_directory\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pcc_directory\Enums\Endpoints;

class PccDirectoryForm extends ConfigFormBase {

  protected $settingsKey = 'pcc_directory.settings';

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config($this->settingsKey);
    $form[Endpoints::GROUP] = [
      '#type' => 'textfield',
      '#title' => t('Group API Endpoint'),
      '#default_value' => $config->get('endpoints.group'),
      '#size' => 64,
      '#maxlength' => 128,
      '#description' => t('The API endpoint for group (department) retrieval.'),
      '#required' => TRUE,
    ];
    $form[Endpoints::PROFILE] = [
      '#type' => 'textfield',
      '#title' => t('Profile API Endpoint'),
      '#default_value' => $config->get('endpoints.profile'),
      '#size' => 64,
      '#maxlength' => 128,
      '#description' => t('The API endpoint for profile retrieval.'),
      '#required' => TRUE,
    ];
    $form[Endpoints::IMAGE] = [
      '#type' => 'textfield',
      '#title' => t('Image API Endpoint'),
      '#default_value' => $config->get('endpoints.image'),
      '#size' => 64,
      '#maxlength' => 128,
      '#description' => t('The API endpoint for image retrieval.'),
      '#required' => TRUE,
    ];
    return $form;
  }

  public function getFormId(): string {
    return 'pcc-directory-form';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->settingsKey);
    $config->set('endpoints.group', $form_state->getValue(Endpoints::GROUP));
    $config->set('endpoints.profile',
      $form_state->getValue(Endpoints::PROFILE));
    $config->set('endpoints.image', $form_state->getValue(Endpoints::IMAGE));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  protected function getEditableConfigNames() {
    return [
      $this->settingsKey,
    ];
  }

}
