/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/smorken-obfuscate/src/dom.js":
/*!***************************************************!*\
  !*** ./node_modules/smorken-obfuscate/src/dom.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Dom)\n/* harmony export */ });\n/* harmony import */ var _obfuscate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./obfuscate */ \"./node_modules/smorken-obfuscate/src/obfuscate.js\");\n\n\nclass Dom {\n  all_keys = ['attribute', 'overwrite', 'prepend', 'append']\n  data_key = 'smob'\n  attribute = 'href'\n\n  constructor (smob, config) {\n    if (typeof smob === 'undefined') {\n      smob = new _obfuscate__WEBPACK_IMPORTED_MODULE_0__[\"default\"]()\n    }\n    this._set('smob', smob)\n    if (typeof config !== 'undefined') {\n      for (let [key, value] of Object.entries(config)) {\n        this._set(key, value)\n      }\n    }\n  }\n\n  handle (elements, action) {\n    if (!elements || !elements.length) {\n      return\n    }\n    if (typeof action === 'undefined') {\n      action = 'clear'\n    }\n    for (let i = 0; i < elements.length; i++) {\n      let element = elements[i]\n      this.modifyElement(element, action)\n      this.clearDataAttributes(element)\n    }\n  }\n\n  modifyElement (element, action) {\n    let raw = this._getFromElement(element, this._getDataSetKey())\n    if (raw !== false) {\n      let modified = this._action(raw, action)\n      let attribute = this._getFromElement(element, this._getDataSetKey('attribute'), this.attribute)\n      let overwrite = this._getFromElement(element, this._getDataSetKey('overwrite'))\n      if (attribute) {\n        let prepend = this._getFromElement(element, this._getDataSetKey('prepend'), '')\n        let append = this._getFromElement(element, this._getDataSetKey('append'), '')\n        element[attribute] = prepend + modified + append\n      }\n      if (overwrite !== false) {\n        element.innerHTML = modified\n      }\n    }\n  }\n\n  clearDataAttributes (element) {\n    this._removeAttribute(element, this._getDataKey())\n    for (let i = 0; i < this.all_keys.length; i++) {\n      this._removeAttribute(element, this._getDataKey(this.all_keys[i]))\n    }\n  }\n\n  _action (raw, action) {\n    if (typeof action === 'function') {\n      return action(raw)\n    }\n    switch (action.charAt(0).toLowerCase()) {\n      case 'c':\n        return this.smob.clear(raw)\n      case 'o':\n        return this.smob.obfuscate(raw)\n    }\n  }\n\n  _removeAttribute (element, attribute) {\n    element.removeAttribute(attribute)\n  }\n\n  _getDataKey (type, as_array) {\n    if (typeof as_array === 'undefined') {\n      as_array = false\n    }\n    let key = ['data', this.data_key]\n    if (typeof type !== 'undefined') {\n      let exp = type.split('-')\n      for (let i = 0; i < exp.length; i++) {\n        let val = exp[i]\n        if (val) {\n          key.push(exp[i])\n        }\n      }\n    }\n    return as_array ? key : key.join('-')\n  }\n\n  _getDataSetKey (type) {\n    let keys = this._getDataKey(type, true)\n    let new_keys = []\n    for (let i = 1; i < keys.length; i++) {\n      let val = keys[i]\n      if (val) {\n        if (i > 1) {\n          val = val.charAt(0).toUpperCase() + val.slice(1)\n        }\n        new_keys.push(val)\n      }\n    }\n    return new_keys.join('')\n  }\n\n  _getFromElement (element, data_key, def) {\n    if (typeof def === 'undefined') {\n      def = false\n    }\n    let item = element.dataset[data_key]\n    if (typeof item === 'undefined') {\n      return def\n    }\n    return item\n  }\n\n  _set (k, v) {\n    this[k] = v\n  }\n}\n\n//# sourceURL=webpack:///./node_modules/smorken-obfuscate/src/dom.js?");

/***/ }),

/***/ "./node_modules/smorken-obfuscate/src/index.js":
/*!*****************************************************!*\
  !*** ./node_modules/smorken-obfuscate/src/index.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Obfuscate\": () => (/* reexport safe */ _obfuscate__WEBPACK_IMPORTED_MODULE_0__[\"default\"]),\n/* harmony export */   \"Dom\": () => (/* reexport safe */ _dom__WEBPACK_IMPORTED_MODULE_1__[\"default\"])\n/* harmony export */ });\n/* harmony import */ var _obfuscate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./obfuscate */ \"./node_modules/smorken-obfuscate/src/obfuscate.js\");\n/* harmony import */ var _dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dom */ \"./node_modules/smorken-obfuscate/src/dom.js\");\n\n\n\n\n\n//# sourceURL=webpack:///./node_modules/smorken-obfuscate/src/index.js?");

/***/ }),

/***/ "./node_modules/smorken-obfuscate/src/obfuscate.js":
/*!*********************************************************!*\
  !*** ./node_modules/smorken-obfuscate/src/obfuscate.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Obfuscate)\n/* harmony export */ });\nclass Obfuscate {\n  rotation = 6\n  split_at = '/'\n\n  constructor (config) {\n    if (typeof config !== 'undefined') {\n      for (let [key, value] of Object.entries(config)) {\n        this._set(key, value)\n      }\n    }\n  }\n\n  obfuscate (str, rotation, split_at) {\n    rotation = this._default(rotation, this.rotation)\n    split_at = this._default(split_at, this.split_at)\n    let split = str.split('')\n    return split.map(function (s) {\n      return (parseInt(s.charCodeAt(0)) + parseInt(rotation) + split.length)\n    }).join(split_at)\n  }\n\n  clear (str, rotation, split_at) {\n    rotation = this._default(rotation, this.rotation)\n    split_at = this._default(split_at, this.split_at)\n    let split = str.split(split_at)\n    return split.map(function (s) {\n      return String.fromCharCode(parseInt(s) - parseInt(rotation) - split.length)\n    }).join('')\n  }\n\n  _set (k, v) {\n    this[k] = v\n  }\n\n  _default(v, def) {\n    if (typeof v === 'undefined') {\n      v = def\n    }\n    return v\n  }\n}\n\n//# sourceURL=webpack:///./node_modules/smorken-obfuscate/src/obfuscate.js?");

/***/ }),

/***/ "./resources/js/obf.js":
/*!*****************************!*\
  !*** ./resources/js/obf.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("const {Obfuscate, Dom} = __webpack_require__(/*! smorken-obfuscate */ \"./node_modules/smorken-obfuscate/src/index.js\");\n\nconst smob = new Obfuscate();\nconst smobdom = new Dom(smob);\n\ndocument.addEventListener('click', function (event) {\n    if (!event.target.matches('.obf-show')) {\n        return;\n    }\n    event.preventDefault();\n    smobdom.modifyElement(event.target, 'clear');\n});\n\n\n//# sourceURL=webpack:///./resources/js/obf.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./resources/js/obf.js");
/******/ 	
/******/ })()
;